const addStrings = (num1, num2) => {
    let numbers = []
    let sum = []
    numbers.push(num1, num2)
    while (numbers.length > 0) {
        let sumRightDigits = numbers.map(n => +n[n.length - 1]).reduce((a, b) => a + b)
        if (sumRightDigits < 10) {
            sum.push(sumRightDigits + "")
            numbers = numbers.map(n => n.length > 1 ? n.slice(0, -1) : "").filter(n => n !== "")
        } else {
            numbers = numbers.map(n => n.slice(0, -1) + "0")
            numbers.push(sumRightDigits + "")
        }
    }
    return sum.reverse().join("")
}