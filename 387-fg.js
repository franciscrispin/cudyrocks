const firstUniqChar = (s) => {
    let hashmap = new Map()
    const len = s.length
    for (let i = 0; i < len; i++) {
        if (hashmap.get(s[i]) === undefined) {
            hashmap.set(s[i], 1)
        } else {
            const num = hashmap.get(s[i]) + 1
            hashmap.set(s[i], num)
        }
    }
    let index = 0
    for (let ch of s) {
        if (hashmap.get(ch) === 1) {
            return index
        } else {
            index++
        }
    }
    return -1
};