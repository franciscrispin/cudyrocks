const calPoints = function (ops) {
    const reducer = (a, b) => a + b
    let validPts = []
    ops.map(i => {
        if (!Number.isNaN(parseInt(i))) {
            validPts.push(parseInt(i))
        } else if (i === "+") {
            validPts.push(validPts.slice(-2).reduce(reducer))
        }
        else if (i === "D") {
            validPts.push(validPts.slice(-1)[0] * 2)
        }
        else {
            validPts.pop()
        }
    })
    return validPts.reduce(reducer)
};