const isValid = (s) => {
    const open = ["(", "{", "["]
    const close = [")", "}", "]"]
    let arr = []
    let idx = 0
    while (idx < s.length) {
        let current = s[idx]
        let lastEl = arr[arr.length - 1]
        if (open.includes(current)) {
            arr.push(current)
        } else if (close.includes(current)) {
            if (close.indexOf(current) === open.indexOf(lastEl)) {
                arr.pop()
            } else {
                arr.push(current)
                break
            }
        }
        idx++
    }
    return arr.length > 0 ? false : true
}